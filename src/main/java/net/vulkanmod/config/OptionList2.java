package net.vulkanmod.config;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.ContainerObjectSelectionList;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.narration.NarratableEntry;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.util.Mth;
import net.vulkanmod.config.widget.OptionWidget;

import java.util.List;
import java.util.Optional;

public class OptionList2 extends ContainerObjectSelectionList<OptionList2.Entry> {

	public OptionList2(Minecraft minecraftClient, int width, int height, int top, int bottom) {
		super(minecraftClient, width, height, top, bottom);
		this.centerListVertically = false;
	}

	public void addButton(OptionWidget widget) {
		this.addEntry(new net.vulkanmod.config.OptionList2.Entry(widget));
	}

	public void addAll(Option<?>[] options) {
		for (int i = 0; i < options.length; i++) {
			this.addEntry(new net.vulkanmod.config.OptionList2.Entry(
					options[i].createOptionWidget((int) (0.1f * width), 0, (int) (0.8f * width), 20)));
			// this.addEntry(new Entry(options[i].createOptionWidget(width / 2 - 155, 0,
			// 200, 20)));
		}
	}

	public boolean mouseClicked(double mouseX, double mouseY, int button) {
		this.updateScrollingState(mouseX, mouseY, button);
		if (!this.isMouseOver(mouseX, mouseY)) {
			return false;
		} else {
			OptionList2.Entry entry = this.getEntryAtPos(mouseX, mouseY);
			if (entry != null) {
				if (entry.mouseClicked(mouseX, mouseY, button)) {
					this.setFocused(entry);
					this.setDragging(true);
					return true;
				}
			} else if (button == 0) {
				this.clickedHeader((int) (mouseX - (double) (this.getX() + this.width / 2 - this.getRowWidth() / 2)),
						(int) (mouseY - (double) this.getY()) + (int) this.getScrollAmount() - 4);
				return true;
			}

			return false;
		}
	}

	protected net.vulkanmod.config.OptionList2.Entry getEntryAtPos(double x, double y) {
		int i = this.getRowWidth() / 2;
		int j = this.getX() + this.width / 2;
		int k = j - i;
		int l = j + i;
		int m = Mth.floor(y - (double) this.getY()) - this.headerHeight + (int) this.getScrollAmount() - 4;
		int n = m / this.itemHeight;
		if (x < this.getScrollbarPosition() && x >= (double) k && x <= (double) l && n >= 0 && m >= 0
				&& n < this.getItemCount()) {
			return (net.vulkanmod.config.OptionList2.Entry) this.children().get(n);
		}
		return null;
	}

	@Override
	public int getRowWidth() {
		return this.width;
	}

	@Override
	protected int getScrollbarPosition() {
		// return this.width / 2 + 124 + 32;
		return (int) (this.width * 0.95f);
	}

	public Optional<OptionWidget> getHoveredButton(double mouseX, double mouseY) {
		for (net.vulkanmod.config.OptionList2.Entry buttonEntry : this.children()) {
			if (!buttonEntry.button.isMouseOver(mouseX, mouseY))
				continue;
			return Optional.of(buttonEntry.button);
		}
		return Optional.empty();
	}

	protected void renderList(GuiGraphics guiGraphics, int x, int y, int mouseX, int mouseY, float delta) {
		int i = this.getItemCount();
		Tesselator tesselator = Tesselator.getInstance();
		BufferBuilder bufferBuilder = tesselator.getBuilder();
		for (int j = 0; j < i; ++j) {
			int p;
			int k = this.getRowTop(j);
			int l = this.getRowBottom(j);
			if (l < this.getY() || k > this.getY())
				continue;
			int m = y + j * this.itemHeight + this.headerHeight;
			int n = this.itemHeight - 4;
			net.vulkanmod.config.OptionList2.Entry entry = this.getEntry(j);
			int o = this.getRowWidth();
			if (this.isSelectedItem(j)) {
				p = this.getX() + this.width / 2 - o / 2;
				int q = this.getX() + this.width / 2 + o / 2;

				RenderSystem.setShader(GameRenderer::getPositionShader);
				float f = this.isFocused() ? 1.0f : 0.5f;
				RenderSystem.setShaderColor(f, f, f, 1.0f);
				bufferBuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION);
				bufferBuilder.vertex(p, m + n + 2, 0.0).endVertex();
				bufferBuilder.vertex(q, m + n + 2, 0.0).endVertex();
				bufferBuilder.vertex(q, m - 2, 0.0).endVertex();
				bufferBuilder.vertex(p, m - 2, 0.0).endVertex();
				tesselator.end();
				RenderSystem.setShaderColor(0.0f, 0.0f, 0.0f, 1.0f);
				bufferBuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION);
				bufferBuilder.vertex(p + 1, m + n + 1, 0.0).endVertex();
				bufferBuilder.vertex(q - 1, m + n + 1, 0.0).endVertex();
				bufferBuilder.vertex(q - 1, m - 1, 0.0).endVertex();
				bufferBuilder.vertex(p + 1, m - 1, 0.0).endVertex();
				tesselator.end();

			}
			p = this.getRowLeft();
			entry.render(guiGraphics, j, k, p, o, n, mouseX, mouseY, false, delta);
		}
	}

	protected int getRowBottom(int index) {
		return this.getRowTop(index) + this.itemHeight;
	}

	protected static class Entry
			extends ContainerObjectSelectionList.Entry<net.vulkanmod.config.OptionList2.Entry> {
		final OptionWidget button;

		private NarratableEntry focusedSelectable;

		private Entry(OptionWidget button) {
			this.button = button;
		}

		@Override
		public void render(GuiGraphics guiGraphics, int index, int y, int x, int entryWidth, int entryHeight, int mouseX,
				int mouseY, boolean hovered, float tickDelta) {
			button.y = y;
			button.render(guiGraphics, mouseX, mouseY, tickDelta);
		}

		@Override
		public List<? extends GuiEventListener> children() {
			return List.of(this.button);
		}

		@Override
		public List<? extends NarratableEntry> narratables() {
			return List.of(this.button);
		}
	}
}
